class AddChildrenCountToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :children_count, :integer, default: 0
  end
end
