require 'faker'

FactoryBot.define do
  factory :story do
    account
    tag_names ['foo', 'bar']

    trait :link do
      sequence(:url) { Faker::Internet.url }
      sequence(:title) { Faker::Company.catch_phrase }
    end
  end
end
