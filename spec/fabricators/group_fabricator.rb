Fabricator(:group) do
  name { sequence { |i| "Group #{i}" } }
  slug { sequence { |i| "slug#{i}" } }
  domain "prismo.news"
  supergroup false
end
