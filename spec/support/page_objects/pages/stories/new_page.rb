class Stories::NewPage < SitePrism::Page
  set_url '/stories/new'
  set_url_matcher %r{\/stories(\/new)?\z}

  element :url_field, 'input#story_url'
  element :title_field, 'input#story_title'
  element :tag_list_field, 'input#story_tag_list'
  element :description_field, 'textarea#story_description'
  element :submit_button, '[type="submit"]'
end
