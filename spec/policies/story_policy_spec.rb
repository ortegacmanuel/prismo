require "rails_helper"

describe StoryPolicy do
  subject { described_class }

  let(:user) { Fabricate(:user) }
  let(:admin) { Fabricate(:user_admin) }
  let(:account) { Fabricate(:account, user: user) }
  let(:story) { Fabricate(:story, account: account) }

  shared_examples 'is prohibited for guests' do
    it { expect(subject).to_not permit(nil, story) }
  end

  shared_examples 'is available for guests' do
    it { expect(subject).to permit(nil, story) }
  end

  shared_examples 'is available for any non-guest user' do
    it { expect(subject).to permit(Fabricate(:user), story) }
  end

  shared_examples 'is prohibitet for user other than story author' do
    it { expect(subject).not_to permit(Fabricate(:account).user, story) }
  end

  shared_examples 'is available for story author' do
    it { expect(subject).to permit(story.account.user, story) }
  end

  shared_examples 'is available for admin' do
    it { expect(subject).to permit(admin, story) }
  end

  permissions :index? do
    it_behaves_like 'is available for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :create? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :update? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :scrap? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :comment? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end
end
