require "image_processing/mini_magick"

class StoryThumbUploader < Shrine
  include ImageProcessing::MiniMagick

  plugin :processing
  plugin :versions

  process(:store) do |io, context|
    original = io.download
    pipeline = ImageProcessing::MiniMagick.source(original)

    size_200 = pipeline.resize_to_fill!(200, 200)

    original.close!

    {
      original: io,
      size_200: size_200
    }
  end
end
