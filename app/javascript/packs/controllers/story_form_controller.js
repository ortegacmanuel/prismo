import BaseController from './base_controller'
import axios from 'axios'

export default class extends BaseController {
  static targets = ['urlInput', 'titleInput', 'fetchTitleBtn']

  fetchTitle (e) {
    e.preventDefault()
    e.target.classList.add('loading')

    let req = axios.post('/api/v1/tools/scrap_url', {
      url: this.url
    })

    req.then((resp) => {
      this.title = resp.data.title
      this.url = resp.data.url
      e.target.classList.remove('loading')
    })

    req.catch((resp) => {
      e.target.classList.remove('loading')
    })
  }

  handleUrlChange () {
    this.urlDisabled = this.urlInputTarget.value.length == 0
  }

  get url () {
    return this.urlInputTarget.value
  }

  set title (title) {
    this.titleInputTarget.value = title
  }

  set url (url) {
    this.urlInputTarget.value = url
  }

  set urlDisabled (disable) {
    disable ?
      this.fetchTitleBtnTarget.setAttribute('disabled', 'disabled') :
      this.fetchTitleBtnTarget.removeAttribute('disabled')
  }
}
