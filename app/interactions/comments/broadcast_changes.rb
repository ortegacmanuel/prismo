class Comments::BroadcastChanges < ActiveInteraction::Base
  object :comment

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'comments.updated',
      data: Serializer.new(comment, serializer: REST::CommentSerializer)
    }
  end
end
