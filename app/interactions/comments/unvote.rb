class Comments::Unvote < ActiveInteraction::Base
  object :comment
  object :account

  def execute
    vote = ::Vote.find_by(account: account, voteable: comment)
    vote.destroy if vote.present?

    # Stories::BroadcastChanges.run! story: story.reload

    vote
  end
end
