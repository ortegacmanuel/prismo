class Comments::BroadcastCreation < ActiveInteraction::Base
  object :comment

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'comments.created',
      data: Serializer.new(comment, serializer: REST::CommentSerializer)
    }
  end
end
