class Comments::Create < ActiveInteraction::Base
  integer :parent_id, default: nil
  integer :story_id
  string :body, default: nil
  object :account

  validates :body, presence: true

  def execute
    parent = parent_id.present? ? Comment.find(parent_id) : nil
    story  = Story.find(story_id)

    comment = Comment.new
    comment.body = body
    comment.story_id = story.id
    comment.parent_id = parent.id if parent.present?
    comment.account = account

    if comment.save
      Comments::Vote.run(comment: comment, account: account)

      after_comment_create(comment)
    else
      errors.merge!(comment.errors)
    end

    comment
  end

  private

  def after_comment_create(comment)
    comment.cache_depth
    comment.cache_body

    Comments::BroadcastCreation.run! comment: comment
  end
end
