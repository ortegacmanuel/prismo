class Comments::ToggleVote < ActiveInteraction::Base
  object :comment
  object :account

  def execute
    if account.voted_on?(comment)
      compose(Comments::Unvote, inputs)
    else
      compose(Comments::Vote, inputs)
    end
  end
end
