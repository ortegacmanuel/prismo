require 'active_interaction'

class Stories::Create < Stories::CreateUpdateBase
  def execute
    story = Story.new(inputs)
    story.account = account

    story.domain = URI.parse(url).host if url.present?
    story.groups = [Group.find_by!(supergroup: true)]
    story.tag_names = tags

    if story.save
      after_story_save_hook(story)
    else
      errors.merge!(story.errors)
    end

    story
  end

  private

  def after_story_save_hook(story)
    Stories::Vote.run!(story: story, account: account)
    Stories::ScrapJob.perform_later(story.id)
  end
end
