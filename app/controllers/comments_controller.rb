class CommentsController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_comment_ids

  def index
    comments = CommentsQuery.new.hot
    comments = CommentsQuery.new(comments).with_story
    @comments = comments.page(params[:page])
  end

  def recent
    comments = CommentsQuery.new.recent
    comments = CommentsQuery.new(comments).with_story
    @comments = comments.page(params[:page])
    render :index
  end

  def show
    set_account_upvoted_story_ids

    @comment = find_comment
    @children = @comment.children.includes(:account).hash_tree
  end

  private

  def find_comment
    Comment.find(params[:id])
  end
end
