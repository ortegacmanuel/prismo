class ApiController < ActionController::Base
  include Pundit

  skip_before_action :verify_authenticity_token

  serialization_scope :current_account

  helper_method :current_account

  private

  def current_account
    current_user&.account
  end
end
