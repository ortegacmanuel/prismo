class Accounts::StoriesController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_story_ids

  def hot
    @account = find_account
    @page_title = "Hot stories by #{@account.decorate}"
    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).for_account(@account)

    @stories = stories.page(params[:page])

    render :index
  end

  def recent
    @account = find_account
    @page_title = "Recent stories by #{@account.decorate}"
    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).for_account(@account)

    @stories = stories.page(params[:page])

    render :index
  end

  private

  def find_account
    Account.find_by!(username: params[:username]).decorate
  end
end
