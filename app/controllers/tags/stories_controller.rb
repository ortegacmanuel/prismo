class Tags::StoriesController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_story_ids

  def hot
    @tag = find_tag
    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).tagged_with([@tag.name])

    @stories = stories.page(params[:page])

    render :index
  end

  def recent
    @tag = find_tag
    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).tagged_with([@tag.name])

    @stories = stories.page(params[:page])

    render :index
  end

  private

  def find_tag
    Gutentag::Tag.find_by!(name: params[:tag_name]).decorate
  end
end
