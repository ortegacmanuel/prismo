class Story < ApplicationRecord
  Gutentag::ActiveRecord.call self

  include StoryThumbUploader[:thumb]

  attr_accessor :tag_list

  belongs_to :account, counter_cache: true
  has_many :comments, dependent: :destroy
  belongs_to :url_meta, optional: true
  has_many :votes, as: :voteable
  has_many :group_stories, dependent: :destroy
  has_many :groups, through: :group_stories

  validates :url, allow_blank: true, uniqueness: true
end
